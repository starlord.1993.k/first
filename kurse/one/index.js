// Ваше решение
const content = document.querySelector('#tech')
const technologies = [   
  {type: 'js', title: 'Выучить JavaScript', done:false},
  {type: 'git', title: 'Попрактиковаться с Git',  done: false},
  {type: 'react', title: 'Изучить React', done: false},
  {type: 'nodejs', title: 'Понять NodeJS', done:false},
  {type: 'job', title: 'Устроиться на работу', done: false}

]

function init() { 
      let i = 0
      const it = technologies[i]
      content.innerHTML =technologies.map(toCard).join('')
  }


function toCard (tech1) {
 
const doneClass = tech1.done ? 'checked' : ''
  return `
  <li>
      <label data-type="${tech1.type}">
        <input type="checkbox" ${doneClass} > ${tech1.title}
      </label>
    </li>
  `

}
init()

